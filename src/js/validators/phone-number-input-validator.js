const CoreValidators = require('../core-validators/core-validators');
const InputValidator = require('./input-validator');

class PhoneNumberInputValidator extends InputValidator {
    constructor() {
        super('phone-number', CoreValidators.validatePhoneNumber);
    }
}

module.exports = PhoneNumberInputValidator;