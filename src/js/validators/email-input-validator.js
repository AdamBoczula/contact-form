const CoreValidators = require('../core-validators/core-validators');
const InputValidator  = require('./input-validator');

class EmailInputValidator extends InputValidator {
    constructor() {
        super('email', CoreValidators.validateEmail);
    }
}

module.exports = EmailInputValidator;