class CoreValidators {
    static validateEmail(email) {
        if(!email) return false;
        email = email.trim();
        const emailRegex = /^\s*[\w\-+_]+(\.[\w\-+_]+)*@[\w\-+_]+\.[\w\-+_]+(\.[\w\-+_]+)*\s*$/;
        return emailRegex.test(email);
    }
    static validatePhoneNumber(number) {
        number = number.replace(/ /g, '');
        const phoneNumberRegex = /^\d{9}$/;
        return phoneNumberRegex.test(number);
    }
    static validateName(name) {
        if (!name) return true;
        name = name.trim();
        const nameRegex = /^[a-zA-Z ]{2,30}$/;
        return nameRegex.test(name);
    }
    static validateMessage(message) {
        if (!message) return false;
        const lengthOfComment = message.trim().length;
        return Boolean(lengthOfComment) && lengthOfComment <= 300;
    }
}

module.exports = CoreValidators;