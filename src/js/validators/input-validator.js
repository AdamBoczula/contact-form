const SPACE = ' ';
const errorMessagesConstants = require('./error-messages/error-messages-constants');

class InputValidator {
    constructor(inputClass, validateCallback, isObligatory=true) {
        this.inputClass = inputClass;
        this.validateCallback = validateCallback;
        this.isObligatory = isObligatory;
        this.$element = document.getElementsByClassName(this.inputClass)[0];
        this.$errorInformation = document.getElementsByName(`error-${this.inputClass}`)[0];
        this.faultyText = '';
    }

    validate() {
        const value = this.$element.value;
        if (this.isObligatory && !value) {
            this.faultyText = errorMessagesConstants.obligatory.TEXT;
            return false;
        }
        else if (!this.validateCallback(this.$element.value)) {
            this.faultyText = errorMessagesConstants[this.inputClass].TEXT;
            return false;
        }
        else return true;
    }

    showError() {
        this.$element.className += (SPACE +  'not-valid');
        this.$errorInformation.style.display = 'block';
        this.$errorInformation.innerText = this.faultyText;
    }

    clearStyle() {
        this.$element.className = this.inputClass;
        this.$errorInformation.innerText = '';
    }

    clearInput() {
        this.$element.value = '';
    }
}

module.exports = InputValidator;