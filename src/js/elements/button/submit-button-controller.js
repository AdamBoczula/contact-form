const PhoneNumberInputValidator = require('../../validators/phone-number-input-validator');
const EmailInputValidator = require('../../validators/email-input-validator');
const NameInputValidator = require('../../validators/name-input-validator');
const MessageInputValidator = require('../../validators/message-input-validator');

class SubmitButtonController {
    constructor() {
        this.inputs = [
            new EmailInputValidator(),
            new PhoneNumberInputValidator(),
            new NameInputValidator(),
            new MessageInputValidator()
        ];
    }

    validateAll() {
        let isEveryCorrect = true;

        this.inputs.forEach((input) => {
            if (!input.validate()) {
                isEveryCorrect = false;
                input.showError();
            } else input.clearStyle();
        });

        return isEveryCorrect;
    }

    setFocusOnFirstInvalid() {
        const firstFaultyElement = this.inputs.find((input) => {
            return !input.validate();
        });
        firstFaultyElement.$element.focus();
    }
    clearInputs() {
        this.inputs.forEach((input) => input.clearInput())
    }
}

module.exports = SubmitButtonController;