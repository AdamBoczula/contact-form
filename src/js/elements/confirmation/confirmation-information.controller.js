const CONFIRMATION_TEXT = 'Dziękujemy za komentarz';

class ConfirmationInformationController {
    constructor() {
        this.$element = document.getElementsByClassName('confirmation-information')[0];
    }
    showConfirmation() {
        this.$element.innerText = CONFIRMATION_TEXT;
    }
    clearConfirmation() {
        this.$element.innerText = '';
    }
}

module.exports = ConfirmationInformationController;