const CoreValidators = require('./core-validators');

describe('CoreValidators: ', () => {
    describe('Email validator', () => {
        test('should return false when email is empty', () => {
            expect(CoreValidators.validateEmail()).toBe(false);
        });
        describe('Local part', () => {
            test('should return true when email is Adam.Boczula@hotmail.com', () => {
                expect(CoreValidators.validateEmail('Adam.Boczula@hotmail.com'))
                    .toBe(true);
            });
            test('should return true when email is adam_boczula@hotmail.com', () => {
                expect(CoreValidators.validateEmail('adam_boczula@hotmail.com'))
                    .toBe(true);
            });
            test('should return true when email is 1adam@gmail.com', () => {
                expect(CoreValidators.validateEmail('1adam@gmail.com'))
                    .toBe(true);
            });
            test('should return true when email is +adam-b@o2.pl', () => {
                expect(CoreValidators.validateEmail('+adam-b@o2.pl'))
                    .toBe(true);
            });
            test('shuld return true when email is 12312@o2.pl', () => {
                expect(CoreValidators.validateEmail('12312@o2.pl'))
                    .toBe(true);
            });
            test('shuld return true when email is ADAM@o2.pl', () => {
                expect(CoreValidators.validateEmail('ADAM@o2.pl'))
                    .toBe(true);
            });
            test('shuld return true when email is adam@o2.pl', () => {
                expect(CoreValidators.validateEmail('adam@o2.pl'))
                    .toBe(true);
            });
            test('should return false when email is John..Doe@example.com', () => {
                expect(CoreValidators.validateEmail('John..Doe@example.com'))
                    .toBe(false);
            });
            test('should return false when email is @o2.pl', () => {
                expect(CoreValidators.validateEmail('@example.com'))
                    .toBe(false);
            });
        });
        describe('Domain part', () => {
            test('should return false when email is email@', () => {
                expect(CoreValidators.validateEmail('email@'))
                    .toBe(false);
            });
            test('should return true when email is email@ONET.pl', () => {
                expect(CoreValidators.validateEmail('email@ONET.pl'))
                    .toBe(true);
            });
            test('should return true when email is email@o-c.pl', () => {
                expect(CoreValidators.validateEmail('email@0-c.pl'))
                    .toBe(true);
            });
            test('should return true when email is email@4.pl', () => {
                expect(CoreValidators.validateEmail('email@4.pl'))
                    .toBe(true);
            });
            test('should return false when email is email@.pl', () => {
                expect(CoreValidators.validateEmail('email@.pl'))
                    .toBe(false);
            });
        })
    });
    describe('Phone number validator', () => {
        test('should return true when number contains 9 digits', () => {
            expect(CoreValidators.validatePhoneNumber('123456789'))
                .toBe(true);
        });
        // TODO:
        // Ask client if option with country directional number is needed
        test.skip('should return true when number contains 9 digits', () => {
            expect(CoreValidators.validatePhoneNumber('+48123456789'))
                .toBe(true);
        });
        test('should return true when number contains spaces digits', () => {
            expect(CoreValidators.validatePhoneNumber('123 456 789'))
                .toBe(true);
        });
        test('should return false when number contains 9 char', () => {
            expect(CoreValidators.validatePhoneNumber('aaabbbccc'))
                .toBe(false);
        });
        test('should return false when number contains 8 digits', () => {
            expect(CoreValidators.validatePhoneNumber('12345678'))
                .toBe(false);
        });
        test('should return false when number contains 10 digits', () => {
            expect(CoreValidators.validatePhoneNumber('1234567890'))
                .toBe(false);
        });
    });
    describe('Name validator', () => {
        test('should return true when name is undefined', () => {
            expect(CoreValidators.validateName()).toBe(true);
        });
        test('should return true when name is empty', () => {
            expect(CoreValidators.validateName('')).toBe(true);
        });
        test('should return true when name contains 30 signs', () => {
            expect(CoreValidators.validateName('a'.repeat(30))).toBe(true);
        });
        test('should return true when name contains Adam Jan boczula', () => {
            expect(CoreValidators.validateName('Adam Jan boczula')).toBe(true);
        });
        test('should return true when name is Adam BOCZULA', () => {
            expect(CoreValidators.validateName('Adam BOCZULA')).toBe(true);
        });
        test('should return true when name is adam boczula', () => {
            expect(CoreValidators.validateName('adam boczula')).toBe(true);
        });
        test('should return true when name is adab sdfw', () => {
            expect(CoreValidators.validateName('adab sdfw')).toBe(true);
        });
        test('should return true when name is \' adam boczula  \'', () => {
            expect(CoreValidators.validateName(' adam boczula  ')).toBe(true);
        });
        test('should return false when name contains 31 signs', () => {
            expect(CoreValidators.validateName('A'.repeat(31))).toBe(false);
        });
        test('should return false when name contains 1 digits', () => {
            expect(CoreValidators.validateName('A3 Boczula')).toBe(false);
        });
    });
    describe('Message validator', () => {
        test('should return false when message is empty', () => {
            expect(CoreValidators.validateMessage('')).toBe(false);
        });
        test('should return false when message is undefined', () => {
            expect(CoreValidators.validateMessage()).toBe(false);
        });
        test('should return false when message contains more than 300 sign', () => {
            expect(CoreValidators.validateMessage('a'.repeat(301))).toBe(false);
        });
        test('should return false when message contains only whitespace', () => {
            expect(CoreValidators.validateMessage(' \t ')).toBe(false);
        });
        test('should return true when message contains below 300 sign', () => {
            expect(CoreValidators.validateMessage('A3 Boczula')).toBe(true);
        });
    })
});

