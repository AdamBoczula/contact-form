const CoreValidators = require( '../core-validators/core-validators');
const InputValidator = require( './input-validator');
const sendForm = require( '../elements/button/send-form');

const ENTER = "enter";

class MessageInputValidator extends InputValidator {
    constructor() {
        super('message', CoreValidators.validateMessage);
        this.$element.addEventListener('keypress', ($event) => {
            if($event.key.toLowerCase() === ENTER) sendForm();
        });
    }
}

module.exports = MessageInputValidator;