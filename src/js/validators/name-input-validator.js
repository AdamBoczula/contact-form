const CoreValidators = require('../core-validators/core-validators');
const InputValidator = require('./input-validator');

class NameInputValidator extends InputValidator {
    constructor() {
        super('name', CoreValidators.validateName, false);
    }
}

module.exports = NameInputValidator;