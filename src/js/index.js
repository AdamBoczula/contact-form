require('../styles/wrapper.scss');
require('../styles/element.scss');
require('../styles/input-form.scss');
require('../styles/body.scss');
require('../styles/submit-button.scss');
require('../styles/main-window.scss');
require('../styles/message.scss');
require('../styles/error.scss');

require('./elements/button/send-form');
