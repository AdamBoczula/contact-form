# Contact form
### Dodaj instrukcję z informacją jak zbuować projekt

Utwórz formularz kontaktowy składający się z pól:
* email (wymagane, poprawny email)
* telefon (wymagane, poprawny telefon - xxxxxxxxx, gdzie x jest cyfrą)
* imię i nazwisko (nie wymagane, maksymalnie 30 znaków)
* wiadomość (wymagane, maksymalnie 300 znaków

Używaj:
* HTML5
* CSS3

Wygląd:
* Dwie kolumny 
    * po lewo pierwsze trzy pola
    * po prawej pole wiadomości
* Mobilne zaczynają się od 800px, a kolumny powinny się schodzić jedna pod drugą

Walidacja:
* po stronie JavaScript ES6
* brak frameworków / bibliotek

Używaj:
* LESS/SASS

Dodatkowe informacje:
* po wysłaniu poprawnego formularza:
    * wyświetlona jest stosowna informacja nad formularzem (wysłałeś prawidłowo formularz), a sam formularz powinien się wyczyścić
* po wysłaniu błędnego formularza:
    * zaczynamy od pola, które jest niepoprawnie wypełnione
    * scrollujemy widok do pierwszego źle wypełnionego pola
    * przy każdym błędnym polu pokazujemy co jest nie tak (pole wymagane, za dużo znaków...)
* kod ES6 powinien być kompilowany do ES5 przy użyciu webpack / gulp / grunt
* 
##### Aby zbudować aplikację wpisz:
```
yarn install
```

##### Aby uruchomić aplikację wpisz:
```
yarn start
```
oraz otwórz plik `index.html` znajdujący się w katalogu `/src`

##### Aby uruchomić testy jednostkowe aplikacji wpisz:
```
yarn test
```