const SubmitButtonController = require('./submit-button-controller');
const ConfirmationInformationController = require('../confirmation/confirmation-information.controller');

const submitButtonController = new SubmitButtonController();
const confirmationInformationController = new ConfirmationInformationController();

submitButtonController.clearInputs();
confirmationInformationController.clearConfirmation();
const $button = document.getElementsByClassName('submit-button')[0];
$button.addEventListener('click', sendForm);

function sendForm() {
    if (submitButtonController.validateAll()) {
        submitButtonController.clearInputs();
        confirmationInformationController.showConfirmation();
    } else submitButtonController.setFocusOnFirstInvalid();
}

module.exports = sendForm;